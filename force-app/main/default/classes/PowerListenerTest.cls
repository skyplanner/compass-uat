/**
 * Test class for: PowerListener
 * @author fernando Gomez, SkyPlanner LLC
 */
@isTest
private class PowerListenerTest {
	/**
	 * Tests: global static void acceptPost()
	 */
	@isTest
	static void acceptPost() {
		QuickbookAccount__mdt qba;
		String verifyToken, payload, intuitSignature;
		QbEstimate estimate;
		QbEstimate.QbEstimateResponse qResponse;
		QbHttpMockup mockup;
		RestRequest request;
		RestResponse response;

		verifyToken = '8a72cfbf-7c26-4fb8-b7f0-e2abf244b2ce';
		payload = '{"eventNotifications":[{"realmId":"4620816365048867070"' +
			',"dataChangeEvent":{"entities":[{"name":"Estimate","id":"157",' +
			'"operation":"Emailed","lastUpdated":"2020-07-30T22:20:41.000Z"}]}}]}';
		intuitSignature = 'XMEPF3ET/8YPTXIwgEoGieqfCRpnnAPBvIESyz44/Ig=';

		estimate = new QbEstimate('157', '0', null,
			null, 4500, null, null, null, null, null);
		estimate.TxnStatus = 'Pending';
		estimate.EmailStatus = 'Emailed';
		qResponse = new QbEstimate.QbEstimateResponse();
		qResponse.Estimate = estimate;
		mockup = new QbHttpMockup(200, null, JSON.serialize(qResponse));
		Test.setMock(HttpCalloutMock.class, mockup);
		
		request = new RestRequest();
		request.headers.put('intuit-signature', intuitSignature);
		request.requestBody = Blob.valueOf(payload);

		response = new RestResponse();

		RestContext.request = request;
		RestContext.response = response;
		QuickbookAccount.testVerificationToken = verifyToken;

		// let's call this stuff
		Test.startTest();
		PowerListener.acceptPost();
		System.assertEquals(200, RestContext.response.statusCode);
		Test.stopTest();
	}

	/**
	 * Tests: global static void acceptPost()
	 */
	@isTest
	static void acceptPost_fail() {
		QuickbookAccount__mdt qba;
		String verifyToken, payload, intuitSignature;
		QbEstimate estimate;
		QbEstimate.QbEstimateResponse qResponse;
		QbHttpMockup mockup;
		RestRequest request;
		RestResponse response;

		verifyToken = '8a72cfbf-7c26-4fb8-b7f0-e2abf244b2ce';
		payload = '{"eventNotifications":[{"realmId":"4620816365048867070"' +
			',"dataChangeEvent":{"entities":[{"name":"Estimate","id":"157",' +
			'"operation":"Emailed","lastUpdated":"2020-07-30T22:20:41.000Z"}]}}]}';
		intuitSignature = 'XMEPF3ET/8YPTXIwgEoGieqfCRpnnAPBvINVALID/Ig=';

		estimate = new QbEstimate('157', '0', null,
			null, 4500, null, null, null, null, null);
		estimate.TxnStatus = 'Pending';
		estimate.EmailStatus = 'Emailed';
		qResponse = new QbEstimate.QbEstimateResponse();
		qResponse.Estimate = estimate;
		mockup = new QbHttpMockup(200, null, JSON.serialize(qResponse));
		Test.setMock(HttpCalloutMock.class, mockup);
		
		request = new RestRequest();
		request.headers.put('intuit-signature', intuitSignature);
		request.requestBody = Blob.valueOf(payload);

		response = new RestResponse();

		RestContext.request = request;
		RestContext.response = response;
		QuickbookAccount.testVerificationToken = verifyToken;

		// let's call this stuff
		Test.startTest();
		PowerListener.acceptPost();
		System.assertEquals(401, RestContext.response.statusCode);
		Test.stopTest();
	}
}
