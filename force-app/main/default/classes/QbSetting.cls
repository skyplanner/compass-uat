/**
 * Represent a record of the custom metadata QbSetting
 * parsed and ready to use. Contains static methods to get
 * records of this custom metadata.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 6/19/2020
 */
public class QbSetting {
	public String name { get; private set; }
	public String value { get; private set; }
	public Map<String, String> placeholderMap { get; private set; }

	// allows us to know if the seetings have been fetched
	// so we only perform a query
	private static Map<String, QbSetting> settings;

	/**
	 * @param cm
	 */
	private QbSetting(QbSettings__mdt qbs) {
		name = qbs.DeveloperName;
		value = qbs.Value__c;

		try {
			placeholderMap = (Map<String, String>)
				JSON.deserialize(
					qbs.PlaceholderMappingJson__c,
					Map<String, String>.class);
		} catch (Exception ex) {
			placeholderMap = null;
		}
	}

	/**
	 * @param name
	 * @return the setting with the specified name
	 */
	public static QbSetting getByName(String name) {
		fetch();
		return settings.get(name);
	}

	/**
	 * Obtains the records from db if the haven't been obtain
	 * in the current context already.
	 */
	private static void fetch() {
		if (settings == null) {
			settings = new Map<String, QbSetting>();
			for (QbSettings__mdt qbs : getMdRecords())
				settings.put(qbs.DeveloperName, new QbSetting(qbs));
		}
	}

	/**
	 * @return all qb settings the db
	 */
	private static List<QbSettings__mdt> getMdRecords() {
		return [
			SELECT 
				DeveloperName, 
				Value__c,
				PlaceholderMappingJson__c
			FROM QbSettings__mdt
		];
	}
}
