/**
 * Represents a quickbook reference to object type.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 6/18/2020
 */
public class QbTxReference {
	public String TxnId { get; set; }
	public String TxnType { get; set; }
}
