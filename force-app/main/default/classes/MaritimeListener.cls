/**
 * Listens to events from the Maritime quickbook app.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 7/7/2020
 */
@RestResource(urlMapping='/maritime-events')
global without sharing class MaritimeListener extends EventListener {
	/**
	 * Main constructor
	 */
	global MaritimeListener() {
		super(QuickbookAccount.getByName('QbMaritime'), 'Maritime');
		// we now proceed to process the evetns
		processEvents();
	}

	/**
	 * All events come as a POST request.
	 */
	@HttpPost
	global static void acceptPost() {
		MaritimeListener listener;

		try {
			listener = new MaritimeListener();
			RestContext.response.statusCode = 200;
		} catch (QbExceptions.BaseException ex) {
			RestContext.response.statusCode = ex.getStatusCode();
		}
	}
}
