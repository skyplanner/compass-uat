/**
 * Test class for QuickbookAccount
 * @author fernando Gomez, SkyPlanner LLC
 */
@isTest
private class QuickbookAccountTest {
	/**
	 * Tests: public static QuickbookAccount getByName(String name)
	 */
	@isTest
	static void getByName() {
		QuickbookAccount solar, power, maritime;

		solar = QuickbookAccount.getByName('QbSolar');
		power = QuickbookAccount.getByName('QbPower');
		maritime = QuickbookAccount.getByName('QbMaritime');

		System.assert(solar != null);
		System.assert(power != null);
		System.assert(maritime != null);
	}
}
