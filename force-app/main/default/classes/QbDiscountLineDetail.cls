/**
 * Represents a quickbook estimate dicount line item.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 6/18/2020
 */
public class QbDiscountLineDetail extends QbJsonSerializable {
	public QbReference DiscountAccountRef { get; set; }
	public Boolean PercentBased { get; set; }
	public Decimal DiscountPercent { get; set; }
}
