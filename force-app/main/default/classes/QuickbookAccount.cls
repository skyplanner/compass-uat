/**
 * Provides access to the QuickbookAccount__mtd object.
 */
public class QuickbookAccount {
	public String dashboardUrl { get; private set; }
	public String namedCredential { get; private set; }
	public String realmId { get; private set; }
	public String verificationToken { get; private set; }
	public List<String> listenerSharedtypes { get; private set; }

	// allows us to know if the seetings have been fetched
	// so we only perform a query
	private static Map<String, QuickbookAccount> accounts;

	@TestVisible
	private static String testVerificationToken;
	
	/**
	 * @return the record with the 
	 * specified developer name
	 */
	public static QuickbookAccount getByName(String name) {
		fetch();
		return accounts.get(name);
	}

	/**
	 * @param cm
	 */
	private QuickbookAccount(QuickbookAccount__mdt qacc) {
		dashboardUrl = qacc.DashboardUrl__c;
		namedCredential = qacc.NamedCredential__c;
		realmId = qacc.RealmId__c;
		listenerSharedtypes = qacc.ListenerSharedTypes__c.split(';');

		// we need to mimic the verification token on tests
		// since validation is a bit complicated to achieve
		verificationToken = Test.isRunningTest() && 
				String.isNotBlank(testVerificationToken) ?
			testVerificationToken : qacc.VerificationToken__c;
	}

	/**
	 * Obtains the records from db if the haven't been obtain
	 * in the current context already.
	 */
	private static void fetch() {
		if (accounts == null) {
			accounts = new Map<String, QuickbookAccount>();
			for (QuickbookAccount__mdt qacc : getMdRecords())
				accounts.put(qacc.DeveloperName, new QuickbookAccount(qacc));
		}
	}

	/**
	 * @return all qb settings the db
	 */
	private static List<QuickbookAccount__mdt> getMdRecords() {
		return [
			SELECT DashboardUrl__c,
				NamedCredential__c,
				RealmId__c,
				VerificationToken__c,
				DeveloperName,
				ListenerSharedTypes__c
			FROM QuickbookAccount__mdt
		];
	}
}
