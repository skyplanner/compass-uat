/**
 * Represents a quickbook estimate sales line item details.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 6/18/2020
 */
public class QbSalesItemLineDetail {
	public QbReference TaxCodeRef { get; set; }
	public QbReference ItemRef { get; set; }

	public Decimal Qty { get; set; }
	public Decimal UnitPrice { get; set; }

	/**
	 * @param TaxCodeRef
	 * @param ItemRef
	 * @param Qty
	 * @param UnitPrice
	 */
	public QbSalesItemLineDetail(QbReference TaxCodeRef,
			QbReference ItemRef, Decimal Qty, Decimal UnitPrice) {
		this.TaxCodeRef = TaxCodeRef;
		this.ItemRef = ItemRef;
		this.Qty = Qty;
		this.UnitPrice = UnitPrice;
	}
}
