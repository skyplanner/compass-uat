/**
 * Looksup products (items) in Quickbooks and returns the search result.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 6/24/2020
 */
public class QuickbookProductSearchCtrl extends Integrator {

	/**
	 * @param businessType
	 */
	private QuickbookProductSearchCtrl(String businessType) {
		super(businessType);
	}

	/**
	 * Finds products (items) containing the text
	 * passed in the criteria parameter.
	 * @param criteria
	 */
	private List<QbItem> searchProducts(String criteria) {
		return connector.findActiveItems(criteria);
	}

	/**
	 * Saves the product Id and synch token to the opportunity.
	 * @param opportunityId
	 * @param product
	 * @param qty
	 */
	private void save(Id opportunityId, QbItem product, Integer qty) {
		Opportunity opp;

		// we need the opp
		opp = getOpportunity(opportunityId);

		// the we add values from the item
		opp.QbProductId__c = product.Id;
		opp.QbProductQuantity__c = qty;
		opp.QbProductName__c = product.FullyQualifiedName;
		opp.QbProductDescription__c = product.Description;
		opp.QbProductUnitPrice__c = product.UnitPrice;
		update opp;
	}

	/**
	 * @return the current opportunity
	 */
	private Opportunity getOpportunity(Id opportunityId) {
		return [
			SELECT Id
			FROM Opportunity
			WHERE Id = :opportunityId
		];
	}

	/**
	 * @param businessType
	 * @param criteria
	 */
	@AuraEnabled
	public static List<QbItem> searchProducts(String businessType, String criteria) {
		try {
			QuickbookProductSearchCtrl qpsc = 
				new QuickbookProductSearchCtrl(businessType);
			return qpsc.searchProducts(criteria);
		} catch (Exception ex) {
			throw new AuraHandledException(ex.getMessage());
		}
	}

	/**
	 * @param businessType
	 * @param opportunityId
	 * @param product
	 * @param qty
	 */
	@AuraEnabled
	public static void saveDefaultProduct(String businessType,
			Id opportunityId, QbItem product, Integer qty) {
		try {
			QuickbookProductSearchCtrl qpsc = 
				new QuickbookProductSearchCtrl(businessType);
			qpsc.save(opportunityId, product, qty);
		} catch (Exception ex) {
			throw new AuraHandledException(ex.getMessage());
		}
	}
}