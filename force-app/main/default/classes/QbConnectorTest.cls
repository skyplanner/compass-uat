/**
 * Test class for: CustomerIntegrator
 * @author fernando Gomez, SkyPlanner LLC
 */
@isTest
private class QbConnectorTest {
	/**
	 * Tests: public QbCustomer getCustomer(String customerId)
	 */
	@isTest
	static void getCustomer() {
		QbHttpMockup mockup;
		QbCustomer.QbCustomerResponse response;
		QbCustomer customer;
		QbConnector connector;

		customer = new QbCustomer('001', '0', 'acc.Name', null, 'ctc.FirstName',
			null, 'ctc.LastName', 'acc.Name', '(305) 234-0090', null, null, null,
			null, null, null, null, null);
		response = new QbCustomer.QbCustomerResponse();
		response.Customer = customer;
		mockup = new QbHttpMockup(200, null, JSON.serialize(response));
		Test.setMock(HttpCalloutMock.class, mockup);

		connector = new QbConnector('QbSolar', '1223344546');
		customer = connector.getCustomer('001');

		System.assert(customer != null);
	}

	/**
	 * Tests: public QbEstimate updateEstimate(QbEstimate estimate)
	 */
	@isTest
	static void updateEstimate() {
		QbHttpMockup mockup;
		QbEstimate.QbEstimateResponse response;
		QbEstimate estimate;
		QbConnector connector;

		estimate = new QbEstimate('002', '0', null,
			null, 4500, null, null, null, null, null);
		response = new QbEstimate.QbEstimateResponse();
		response.Estimate = estimate;
		mockup = new QbHttpMockup(200, null, JSON.serialize(response));
		Test.setMock(HttpCalloutMock.class, mockup);

		connector = new QbConnector('QbPower', '1223344546');
		estimate = connector.updateEstimate(estimate);

		System.assert(estimate != null);
	}
}
