({
	doInit: function(component, event, helper) {
		// we start the flow...
		component.find("flowData")
			.startFlow("Primary_Contact_Selector", [{
				name: "recordId",
				type: "String",
				value: component.get("v.recordId") 
			}]);
	}, 
	handleInit: function(component, event, helper) {
		var lastModified = component.get("v.lastModified"),
			newLastModified,
			refresh = function() {
				// we update the record to get new info
				var rl = component.find('recordLoader');
				if (rl)
					rl.reloadRecord(true);
			};
		
		lastModified = component.get("v.lastModified");
		newLastModified = component.get("v.simpleRecord").LastModifiedDate;
		console.log("run " + lastModified);

		if (!lastModified) {
			component.set("v.lastModified", newLastModified);
			setInterval(refresh, 1000);
		} else {
			newLastModified = component.get("v.simpleRecord").LastModifiedDate;
			
			// if it was modified after last time we recorded
			if (newLastModified > lastModified) {
				// we update the last modified
				component.set("v.lastModified", newLastModified);
				// and we launch a refresh view, so each flow is refreshed
				console.log("refreshed");
				component.set("v.showFlow", false);

				// we wait a some time and the create the flow again
				setTimeout(function() {
					component.set("v.showFlow", true);
					component.find("flowData")
						.startFlow("Primary_Contact_Selector", [{
							name: "recordId",
							type: "String",
							value: component.get("v.recordId") 
						}]);
				}, 500);
			}
		}
	}
})
