({
	handleSelectProduct: function (component, event, helper) {
		var modalBody, modalFooter;

		$A.createComponents([[
				"c:QuickbookProductSearch", {"recordId": component.get("v.recordId")}
			]],
			function (components, status) {
				if (status === "SUCCESS") {
					modalBody = components[0];
					modalFooter = components[1];
					component.find('overlayLib').showCustomModal({
						header: "Search Products",
						body: modalBody,
						footer: modalFooter,
						showCloseButton: true,
						cssClass: "",
						closeCallback: function () {
							component.find('recordLoader').reloadRecord(true);
						}
					})
				}
			}
		);
	}
})
