/**
 * @File Name          : OpportunityTrigger_Test.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/11/2020, 4:11:32 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/11/2020   acantero     Initial Version
**/

@isTest
private class OpportunityTrigger_Test {

    static final String POWER = 'Power';

    @isTest
    static void trigger_test1() {
        Account account1 = new Account(
            Name = 'Test Account 1',
            Business_Type__c = POWER
        );
        insert account1;
        Opportunity opp1 = new Opportunity(
            Name = 'Opp1',
            AccountId = account1.Id,
            CloseDate = System.today().addDays(30),
            StageName = 'Lead Origination'
        );
        Test.startTest();
        insert opp1;
        Test.stopTest();
        opp1 = [select Id, RecordTypeId from Opportunity where Id = :opp1.Id];
        Map<String, Schema.RecordTypeInfo> oppRecordTypeMap = 
            Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName();
        ID powerRecTypeId = oppRecordTypeMap.get(POWER).getRecordTypeId();
        System.assertEquals(powerRecTypeId, opp1.RecordTypeId);
    }

}